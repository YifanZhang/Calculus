# **数学分析 I**
******
# 第一章 实数理论
## (P)1.3 实数公理
+ 序公理：

    (1) 序的传递性：如果a<b,b<c,那么a<c;

    (2) 序与加法的关系: 如果a<b,那么对于任意的c \in R, a+c<b+c;

    (3) 序与乘法的关系: 如果a<b,那么对于任意的c \in R, c>0, ac<bc;

+ 令V=H[a,b]是区间[a,b]上所有函数的集合

+ 定理1.4.1（阿基米德原理）：对于任意给定的实数a>0, b>0, 存在自然数N, 使得对于任意n>0, na>b.

+ **定理1.4.6（确界原理)：实数R中任意一个非空有上界集合必有上确界。**
******
# 第二章 极限理论
## (P)2.3 **区间套** 原理
+ 定义2.3.1：实数轴上一系列闭区间{[an,bn]}称为**区间套**，如果其满足

    a): $\forall n, [a_{n+1},b_{n+1}] \in [a_{n},b_{n}]$

    b): $\lim_{n \rightarrow +\infty} (a_n-b_n)\,=\,0$
+ 定理2.3.2（**区间套** 原理）：如果{[an,bn]}是一 **区间套**，则存在唯一的点c, 使得$c \in \cap^{+\infty}_{n=1}[an,bn]$
## (P)2.4 开覆盖定理
+ 定义2.4.1:  设 S ⊂ Rn 是给定的集合,  由一族以R中开区间构成的集合 $K = {(a_k,b_k)}_{k \in A}$ 称为S的 **开覆盖**，如果$S \subset \cup_{k \in A}(a_k, b_k)$.

- 定义2.4.2: 集合 S ⊂ R 称为 **紧集**, 如果对于 S 的任意开覆盖 $K = {(a_k,b_k)}_{k \in A}$ , 都存在 K 中的有限个元素也覆盖 S.·

+ **定理2.4.1（开覆盖定理）：R中的有界闭集都是紧集**。
## (P)2.5 聚点原理与Bolzano定理
  - 定义2.5.1:  $设 S ⊂ R^n 是给定的集合, 点x \in R 称为集合S的聚点，如果对于任意\epsilon>0, \\x的\epsilon-邻域（x-\epsilon, x+\epsilon）内都包含S中的无穷多个点。$
    - 通常以S' 表示集合S所有聚点组成的集合，S' 称为导集。

  - **定理2.5.1（聚点原理）: R中任意有界无穷集合必有聚点.**
    - 发散数列的上极限和下极限
  - 定义2.5.2：设{a_n}是一给定的序列，点x称为{a_n}的**极限点**，如果x的任意邻域内都含序列{a_n}的无穷多项。
    - **极限点与聚点不同，序列与集合！**
    - 以 $\tilde{A}表示由{a_n}$ 的极限点构成的集合。
  - 定理2.5.2：对于有界序列{a_n}, 其极限点的集合S都不是空集。
  - 引理2.5.1：x为{a_n}的极限点的充分必要条件是存在 ${a_n}的子序列{a_{nk}}, 使得\lim_{k \rightarrow + \infty}a_{nk}=x$.
  - **定理2.5.3（Bolzano定理）：任意有界序列必有收敛子列。**
  - 定义2.5.3：设{a_n}是一有界序列，则其极限点集\tilde{S}中的最大值和最小值分别称为序列{a_n}的上极限和下极限。表示为：
    $$\overline{\lim_{n \rightarrow + \infty}} a_n = max \tilde{S},\,\,\,\,\,\underline{\lim_{n \rightarrow + \infty}} a_n = min \tilde{S}$$
  - 定理2.5.4：设{a_n}是一有界序列，则A，B为其上极限和下极限的充分必要条件为$\forall \epsilon > 0, (B-\epsilon, A+\epsilon)外都仅有{a_n}的有限项，而（B+\epsilon, A-\epsilon）左右两边都包含{a_n}的无穷多项。
## (P)2.6 Cauchy准则
+ 定义2.6.1：序列{a_n}称为Cauchy列，如果 $\forall \epsilon >0, \exists N, s.t.只要n_1, n_2>N, 就成立|a_{n_1}-a_{n_2}|<\epsilon$

+ **定理2.6.1(序列极限的Cauchy准则):序列极限{a_n}为收敛的充分必要条件为{a_n}为Cauchy列。**

******
******
# **数学分析 II**
******
# 第四章 $n$维欧氏空间$R^n$
## (P)4.1 $n$维欧氏空间$R^n$
$$V^n=\{(x^1,...,x^n)|x^i \in R,\,i=1,...,n\}$$
### 内积
$$(A,B)=\sum^n_{i=1} x^i y^i$$
    1). 对称性: 对于任意 A, B ∈ Vn, 成立 (A, B) = (B, A);

    2). 线性性: 对于任意 A, B, C ∈ Vn, c1, c2  ∈ R, 成立 (c1A + c2B, C) =
    c1(A, C) + c2(B, C);

    3). 正定性: 对于任意 A ∈ Vn, 成立 (A, A) ≥ 0, 而 (A, A) = 0 当且仅
    当 A = (0, · · · , 0) = 0.


+ **定理4.1.1( Cauchy-Schwarz 不等式)**: 对于任意向量 A, B ∈ Vn, 成立
|(A, B)| ≤   (A, A)(B, B),
并且其中等式成立当且仅当存在 c ∈ R, 使得 A = cB.

+ **定义4.1.1**:  设
A = (x1, · · · , xn), B = (y1, · · · , yn) ∈ Vn
是任意两个向量, 我们定义向量 A 的长度为
||A|| =    (A, A).
定义向量 A 与 B 之间的夹角 θ 为

$$θ = arccos(\frac{(A, B)}{||A|| · ||B||})$$
+ 如果同时用 A 与 B 表示 Vn 中以原点为起点, 向量 A 与 B 所代表的点, 则定义
点 A 与 B 之间的距离为
$$d(A, B) = ||A − B|| = \sqrt{\sum^n_{i=1} (x^i-y^i)^2} $$

+ **定理4.1.2**：V^n上定义的点之间的距离d(A,B)满足：

    (1) d(A,B) = d(B,A)

    (2) d(A,B) >= 0

    (3) d(A,B) <= d(A,C) + d(C,B)

+ **定义4.1.2** $设A_0 \in R^n是给定的点，\epsilon >0是给定的常数$ ，令
$$B(A_0,\epsilon)=\{X \in R^n|\, |X-A_0|<\epsilon\}$$
$B(A, \epsilon)$ 是以A_0为圆心，$\epsilon$为半径的球，称为 $A_0的\epsilon-$ 球形邻域。
$\lim_{m \rightarrow + \infty} A_m = A_0$等价于对于$A_0的任意\epsilon 球形邻域B(A_0,\epsilon),存在N，使得只要m>N,就成立A_m \in B(A_0,\epsilon)$.

+ **定理4.1.3**： **$lim_{m \rightarrow + \infty}A_m=A_0$的充分必要条件是对于任意i=1,2,...n,成立**
$$\lim_{m \rightarrow + \infty} x^i_{m} = a^i_m$$

+ **定义4.1.3**：称集合U为****开集****，如果对于任意$A \in U$, 存在 $\epsilon >0$, 使得 $B(A,\epsilon) \subset U$.

+ **定理4.1.4**：$R^n$ 中的****开集****满足:

    (1)  $R^n$ 和 $R^n$中的空集都是****开集****

    (2) 任意多个**开集**的并仍然是**开集**

    (3) 有限多个**开集**的交仍然是**开集**。

+ **定义4.1.4**：集合$S \subset R^n$ 称为**闭集**，如果$R^n - S$是**开集**。

+ **定理4.1.5**：$R^n$中的**闭集**满足

    (1) $R^n$和 $R^n$ 中的空集都是**闭集**；

    (2) 任意多个**闭集**的交仍然是**闭集**；

    (3) 有限**闭集**多个**闭集**的并仍然是**闭集**；

+ **定义4.1.5**：设$S \subset R^n$ 是任意给定的集合，则R^n 中的点相对于S可以分类定义为：
  + **外点**：$X \in R^n 称为S的外点，如果存在\epsilon >0, 使得B(X, \epsilon) \bigcap S = \emptyset$.
  + **内点**：$X \in R^n 称为S的内点，如果存在\epsilon >0, 使得B(X, \epsilon) \subset S$.
  + **边界点**：$X \in R^n 称为S的边界点，如果存在\epsilon >0, \\使得B(X, \epsilon) 既含有S中的点，也包含不在S中的点$.



+ **定义4.1.6**：设$S \subset R^n$ 是任意给定的集合，
  + **极限点**：$X\in R^n$ 称为S的极限点，如果 $\forall \epsilon >0, B(X,\epsilon)$ 内都含有S中的无穷多个点。通常以S’ 表示S的所有极限点构成的集合，**S’ 称为S的导集**。
  + **孤立点**：$X\in R^n$ 称为S的孤立点，如果 $\exists \epsilon >0, 使得B(X,\epsilon) \bigcap S={X}$.
  + **闭包**：将所有包含S的闭集作交，我们得到的集合仍然是一个闭集，我们称这一闭集为S的闭包，记为 $\overline S$.
  + $\overline S\,=\,S \cup \partial S\,=\,S \cup S'$

+ **定理4.1.6：集合$S \subset R^n$为闭集的充分必要条件是$S' \subset S$**.

+ 集合S称为道路连通的，如如果S中的任意两点都可以用一条包含在S内的连续曲线连接。而利用连通的定义不难看出R^n中任意一个开集都可以分解为有限或者可数多个互不相交的连通开集的并。

+ **定义4.1.8：$R^n 中连通的开集称为区域，区域的闭包则称为闭区域。$**

******
## (P)4.2 $R^n$的完备性
+ 确界原理
+ 单调有界收敛原理
+ **区间套**原理
+ 极限点原理
+ Bolzano 原理
+ Cauchy 准则

+ 定义4.2.1:  Rn 中的序列 {Am} 称为 Cauchy 列,  如果对于任意给定的 ε >
0, 存在 N, 使得只要 m1 > N, m2 > N, 就成立
|Am1   − Am2 | < ε.
+ **定理4.2.1( Cauchy 准则):  Rn 中的序列 {Am} 为收敛序列的充分必要条
件是 {Am} 为 Cauchy 列.**
+ 定义4.2.2: 设 S ⊂ Rn 是给定的集合, 令
diam(S) = sup{|p − q|   p,  q ∈ S}.
diam(S) 称为集合 S 的直径.

+ 定义4.2.3:  Rn 中的一列闭集 {Sm} 称为一个**区间套**, 如果 {Sm} 满足

    1). 对于 m = 1, 2, · · · , Sm $\neq$ ∅, 而 Sm+1 ⊂ Sm;

    2).	$\lim_{m→+∞}$ diam(Sm) = 0.

利用上面定义, 实数轴上的**区间套**原理在 Rn 推广为下面形式的定理.

+ **定理4.2.2(**区间套**原理):  如果闭集序列 {Sm} 是 $R^n$中的一个**区间套**,  则
存在唯一的一个点 $A_0$, 满足 ${A_0} = \bigcap^{+\infty}_{m=1} S_m$**

+ 定义4.2.4:  设 S ⊂ Rn 是给定的集合,  由开集构成的集合 $K = \{U_i\}_{i\in I}$  称
为 S 的开覆盖, 如果 $S ⊂ \bigcup_{i\in I} U_i$. 集合 S ⊂ Rn 称为紧集, 如果对于 S 的任意开覆盖 $K = \{U_i\}_{i\in I}$ , 都存在 K 中的有限个元素也覆盖 S.

+ **定理4.2.3(开覆盖定理): S ⊂ Rn 为紧集的充分必要条件是 S 为有界闭集.**

+ **定理4.2.4（聚点原理）：R^n中任意有界无穷集合都有极限点。**

+ **定理4.2.5（Bolzano定理）：R^n中任意有界序列必有收敛子列。**

******
## (P)4.3 多元函数的极限与多元连续函数
+ 定义4.3.1：设$S\subset R^n, f:S \rightarrow R是定义在S上的函数.$ 设$p_0是S极限点，如果存在A_0 \in R$, 满足 $\forall \epsilon > 0, \exists \delta >0, s.t. 只要p \in S, 且0< |p-p_0|<\delta$, 就成立 $|f(p)-A_0|<\epsilon$, 则称 $p \in S, p \rightarrow p_0时，f(p) \rightarrow A_0,记为 \lim_{p\in S , \,p \rightarrow p_0}=A_0$

+ 定理4.3.1 **（Cauchy准则)**: 设$S\subset R^n, f:S \rightarrow R是定义在S上的函数.$ 设$p_0是S极限点，则p\in S, p \rightarrow p_0$时 $f(p)$ 为收敛的充分必要条件为: $\forall \epsilon >0, \exists \delta>0, s.t.只要|p_1-p_2|<\delta, 就成立|a_{n_1}-a_{n_2}|<\epsilon$.

+ **定理4.3.2：设$x_0 \in (a,b), y_0 \in (c,d)$ , 函数f(x,y)是定义在区域 $D=(a,b) \times (c,d)-{(x_0,y_0)}$ 上的函数，如果单极限 $\lim_{x \rightarrow x_0}$ 收敛，同时重极限 $lim_{(x,y) \rightarrow (x_0,y_0)}$ 也收敛，则累次极限 $\lim_{y \rightarrow y_0} \lim_{x \rightarrow x_0}$ 收敛，且与重极限相等。**

+ 定义4.3.2：设 $S \subset R^n$ 是任意集合，$f: S \rightarrow R$是定义在S上的函数。称$f$在点$p_0 \in S$ 连续，如果 $\forall \epsilon > 0$, 使得只要$p \in S, 且|p-p_0| < \delta$, 就成立 $|f(p)-f(p_0)|<\epsilon$. 如果f在S中的每一点都连续，则称f为集合S上的**连续函数**。

  + 如果p_0 是S的极限点，则f(p)在p_0 连续的充分必要条件是：
  $$\lim_{p \in S, p \rightarrow p_0 }  f(p)=f(\lim_{p \in S, p \rightarrow p_0 } p_0)\,=\,f(p_0)$$

+ 多元向量函数：设$S \subset R^n, 映射f:S \rightarrow R^m, p \rightarrow f(p)为S上$的**连续映射**，如果对于任意点$p_0 \in S,以及点f(p_0)的任意\epsilon邻域B(f(p_0), \epsilon), 存在p_0的\delta 邻域B(p_0,\delta)$, 满足$f(B(p_0,\delta) \cap S,)\subset B(f(p_0),\epsilon).$

  - 不难看出，f在S上连续的充分必要条件是对于i=1,2,...m, S上的多元函数$y^i(x^1,x^2,...,x^n)$

******
## (P)4.4 一元函数三大定理对于多元函数的推广

+ **定理4.4.1（介值定理）：多元连续函数将R^n中连通集映为实轴中的区间。**

+ **定理4.4.2（介值定理）：多元连续向量函数将连通集映为连通的集合。**

+ **定理4.4.3（最大最小值定理）**：设$S\subset R^n是紧集，f: S \rightarrow R$ 是S上的连续函数，则f在S上有界，并且能够取到最大和最小值。

+ **定理4.4.4（最大最小值定理）**：多元连续向量函数将紧集映射为紧集。

+ **定理4.4.5（一致连续定理）**：设$S \subset R^n是紧集，f: S \rightarrow R$是定义在S上的函数，则$f在S$上一致连续。$即对于任意\epsilon >0, 存在\delta >0$, 使得只要$p_1 \in S, p_2 \in S, 满足|p_1-p_2|<\delta, 就成立|f(p_1)-f(p_2)|<\epsilon.$

+ **定理4.4.6（不动点原理）**：设$S \subset R^n是闭集，f: S \rightarrow S$ 是S到S的映射，如果存在实数 $\lambda \in R, 满足 0< \lambda < 1$, 并且对于任意$p_1 \in S, p_2 \in S$, 都成立
$$|f(p_1)-f(p_2)| \leq \lambda |p_1 -p_2|$$
+  则存在唯一的点$p_0 \in S，使得f(p_0)=p_0$. 即f在S上有唯一的不动点。

## (P) 第四章习题
+ [16]
  - (1) $\exists \epsilon >0,  s.t. \forall \delta >0, M>0,\exists x_1,x_2,y_1,y_2满足\\|x_1-x_0|<\delta,|x_2-x_0|<\delta,y_1>M,y_2>M,s.t.|f(x_1,y_1)-f(x_2,y_2)| \geq \epsilon$
  - (2) $\exists \epsilon >0,  s.t. \forall M>0,\exists x_1,x_2,y_1,y_2满足\\x_1<-M,x_2<-M,y_1>M,y_2>M,s.t.|f(x_1,y_1)-f(x_2,y_2)| \geq \epsilon$

+ [17]

+ [18]$\forall \epsilon>0,\exists M>0,s.t.\forall x_1<-M, x_2<-M, y_1>M, y_2>M,|f(x_1,y_1)-f(x_2,y_2)|< \epsilon$

  + 充分性：trivial
  + 必要性: Cauchy列，$|f(x_0,y_0)-A| \leq  |f(x_0,y_0)-f(x_n,y_n)|+|f(x_n,y_n)-A| < 2\epsilon.$

******
******
# 第五章 多元函数的微分学
## (P)5.1 偏导数
+ 设D \in R^2 是开区域，f:D \rightarrow R，(x,y) \rightarrow f(x,y)是定义在D上的函数，(x_0, y_0) \in D 是一给定的点。首先在(x_0, y_0)充分小的邻域上将变量y固定在y_0, 则f(x,y_0)是变量x在x_0充分小邻域上的一元函数，如果这一函数在x_0可导，则称函数f(x,y)在(x_0, y_0)处关于变量x有偏导数，同时将f(x_0,y_0)在x_0处的导数称为f(x,y)在(x_0,y_0)处关于变量x的偏导数。
$$\frac{\partial f}{\partial x} (x_0, y_0)\,=\,f_x(x_0, y_0)\,=\,\lim_{x \rightarrow x_0}{\frac{f(x, y_0)-f(x_0,y_0)}{x-x_0}}$$

+ 定理5.1.1: 如果函数f(x,y)和g(x,y)都在区域$D \subset R^2$ 处处有偏导，且在区域D上处处成立$f_x \equiv g_x, f_y \equiv g_y$, 则存在常数$c \in R$，使得在D上$f(x,y) \equiv g(x,y) +c$. (trivial)

## (P)5.2 全微分
+ 定义5.2.1：设$D \subset R^2$ 是开区域, f: D \rightarrow R, (x,y) \rightarrow f(x,y) 是定义在D上的函数，(x_0,y_0) \in D是一给定的点，如果存在线性函数A(x-x_0)+B(y-y_0), 使得当(x,y) \rightarrow (x_0,y_0)时，
$$f(x,y)-f(x_0,y_0)=A(x-x_0)+ B(y-y_0) +o(|(x,y)-(x_0,y_0)|)$$
则称$f(x,y) 在点(x_0,y_0)$ 处可微，并且称 $Adx+Bdy为f(x,y)在(x_0,y_0)$ 处的微分，记为 $df(x_0,y_0)=Adx+Bdy$.

+ **定理5.2.1: 设f(x,y)在 $(x_0, y_0)的某个邻域上处处有偏导f_x, f_y$ ,并且偏导 $f_x, f_y在点(x_0, y_0)$ 连续，则 $f(x,y)在(x_0, y_0)$ 处可微。**

    + 推论： 设 $f(x,y)在（x_0, y_0)$ 的某个邻域上处处有偏导 $f_x, f_y$, 并且偏导  $f_x, f_y 在点(x_0, y_0)$ 连续，则 $f(x,y)在(x_0, y_0)$ 点连续。
    + 上述定理中偏导数存在且连续的条件仅仅是多元函数可微的一个充分条件，不是必要条件。
    + 对于开区域 $D \subset R^n$, 通常告诉我们用 $C^1(D)$ 表示区域D上处处有连续偏导数的函数全体。上面的讨论告诉我们 $C^1(D)$ 中的函数都是连续函数，并且在D上**处处**可微


+ Leibniz Rule:
$$d^n(uv) = \sum_{k=0}^n {n \choose k} \cdot d^{(n-k)}(u)\cdot  d^{(k)}(v)$$
$$(uv)^{(n)}(x) = \sum_{k=0}^n {n \choose k} \cdot u^{(n-k)}(x)\cdot  v^{(k)}(x)$$

## (P)5.3 高阶偏导
+ 定理
