# Calculus II
## 第七章 重积分
### 二重积分的概念与性质
1. 二重积分的概念

**一个在有界闭区域上连续的二元函数是可积的。**

2. 二重积分的性质

(1)
$$
\iint_D k \cdot f(x,y) d\sigma\,=\,k\iint_D f(x,y) d\sigma
$$

(2)
$$
\iint_D [f(x,y) \pm g(x,y)] d\sigma\,=\,\iint_D f(x,y) d\sigma \pm \iint_D g(x,y) d\sigma
$$

(3)
$$
\iint_D f(x,y) d\sigma\,=\,\iint_{D_1} f(x,y) d\sigma+\iint_{D_2} f(x,y) d\sigma
,\,(D=D_1 \cup D_2)$$

(4)
$$
if: f(x,y) \leq g(x,y),\,\forall (x,y) \in D
\\
\\then: \iint_D f(x,y) d\sigma \leq \iint_D g(x,y) d\sigma
\\
\\Specifically: \iint_D f(x,y) d\sigma \leq
\iint_D \left|f(x,y)\right| d\sigma
$$
(5)
若函数f(x,y)在有界闭区域D上连续，则在D上至少存在一点(x_0,y_0),使得
$$
\iint_D f(x,y) d\sigma\,=\,f(x_0,y_0) \cdot S
$$

3. 典型例题

+ 设函数f(x,y)在有界闭区域D上连续、非负，且
$\iint_D f(x,y) dxdy=0$, 则证明：$f(x,y) \equiv 0,当(x,y)\in D$时。
